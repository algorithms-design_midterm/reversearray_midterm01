/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.kongphop.reversearray;

import java.util.Scanner;

/**
 *
 * @author Admin
 */
public class ReverseArray {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int inputNum = sc.nextInt();
        int[] arr = new int[inputNum];

        inputNumber(inputNum, arr, sc);
        showInput(arr);
        System.out.println("");
        showOutput(arr);
    }

    public static void showOutput(int[] arr) {
        System.out.print("Output: ");
        for (int i = arr.length - 1; i >= 0; i--) {
            System.out.print(arr[i] + " ");

        }
    }

    public static void showInput(int[] arr) {
        System.out.print("Input: ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
            
        }
    }

    public static void inputNumber(int inputNum, int[] arr, Scanner sc) {
        for (int i = 0; i < inputNum; i++) {
            arr[i] = sc.nextInt();
        }
    }
}
